package com.watnapp.etipitaka.plus.adapter;

import android.R;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.google.inject.Inject;

/**
 * Created with IntelliJ IDEA.
 * User: sutee
 * Date: 15/7/2013
 * Time: 14:27
 */

public class PaliDictAdapter extends DictAdapter {

  @Inject
  public PaliDictAdapter(Context context) {
    super(context);
  }

  @Override
  public String getHeadWordColumn() {
    return "headword";
  }

  @Override
  public void setTextViewFontStyle(Context context, TextView textView) {
    Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/THSarabun.ttf");
    textView.setTypeface(font);
    textView.setTextSize(26);
  }

}
